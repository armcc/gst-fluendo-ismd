#!/usr/bin/env python
# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

# -----------------------------------------------------------------------------
# 
#    This file is provided under a dual BSD/LGPLv2.1 license.  When using
#    or redistributing this file, you may do so under either license.
#
#    LGPL LICENSE SUMMARY
#
#    Copyright(c) 2009, 2011. Fluendo S.A. All rights reserved.
#
#    This library is free software; you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation; either version 2.1 of the
#    License.
#
#    This library is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
#    USA. The full GNU Lesser General Public License is included in this
#    distribution in the file called LICENSE.LGPL.
#
#    Contat Information for Fluendo:
#        FLUENDO S.A.
#        World Trade Center Ed Norte 4 pl.
#        Moll de Barcelona
#        08039 BARCELONA - SPAIN
#
#    BSD LICENSE
#
#    Copyright(c) 2009, 2011. Fluendo S.A. All rights reserved.
#
#    Redistribution and use in source and binary forms, with or without
#    modification, are permitted provided that the following conditions
#    are met:
#
#      - Redistributions of source code must retain the above copyright
#        notice, this list of conditions and the following disclaimer.
#      - Redistributions in binary form must reproduce the above copyright
#        notice, this list of conditions and the following disclaimer in
#        the documentation and/or other materials provided with the
#        distribution.
#      - Neither the name of Intel Corporation nor the names of its
#        contributors may be used to endorse or promote products derived
#        from this software without specific prior written permission.
#
#    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import sys, os

import gobject
gobject.threads_init()

# import pygst
# pygst.require('0.10')
import gst
import math

class PiPBin(gst.Bin):
    def __init__(self, uri, plane):
        gst.Bin.__init__(self, 'PiPBin')
        self.linked = False
        src = gst.parse_launch ('uridecodebin caps="video/x-decoded-ismd;video/x-raw-yuv"')
        src.set_property('uri', uri)
        self.add(src)

        # Connect handler for 'pad-added' signal
        src.connect('pad-added', self.on_new_decoded_pad)

        queue = gst.element_factory_make('queue', 'qpip')
        queue.set_property ("max-size-buffers", 3)
        self.sink_pad = queue.get_pad('sink')
        self.add(queue)

        self.vsink = gst.element_factory_make('ismd_vidrend_bin', 'vpip')
        self.vsink.set_property('gdl-plane', plane)
        self.vsink.set_property ("rectangle", "0,0,640,480")
        self.add(self.vsink)

        gst.element_link_many(queue, self.vsink)

    def on_new_decoded_pad(self, element, pad):
        padname = pad.get_name()
        caps = pad.get_caps()
        name = caps[0].get_name()
        gst.info('padname %s %s' % (padname, caps))
        if 'video/' in name and not self.linked:
            pad.link(self.sink_pad)
            self.linked = True

class GstPlayer:
    STOPPED = 0
    PLAYING = 1
    PAUSED = 2
    BUFFERING = 3
    PREROLLING = 4

    def __init__(self, custom_pipeline):
        self.playing = False
        self.on_eos = False
        self._cbuffering = -1
        self.status = self.STOPPED
        self.target_status = self.STOPPED
        self._dir = 1
        self._rate = 1.0

        self.c_video = 0
        self.c_audio = 0
        self.c_text = 0

        self.n_video = 0
        self.n_audio = 0
        self.n_text = 0
        self.PiPBin = None
        self._videosink = None

        if custom_pipeline is None:
            self.player = gst.element_factory_make('playbin2', None)
            self.is_playbin2 = True
            self.player.set_property('flags', 0x43)
            self.player.connect('notify::source', self.on_source_created)
        else:
            self.player = gst.parse_launch (custom_pipeline)
            self.is_playbin2 = False


        bus = self.player.get_bus()
        bus.connect('message::eos', self.on_message_eos)
        bus.connect('message::error', self.on_message_error)
        bus.connect('message::state-changed', self.on_message_state_changed)
        bus.connect('message::buffering', self.on_message_buffering)
        bus.connect('message::clock-lost', self.on_message_clock_lost)
        bus.connect('message::async-done', self.on_message_async_done)
        bus.add_signal_watch()

    def update_rectangle (self, x, y, w, h):
        if self._videosink is not None:
            print "change rectangle on %s %d %d %d %d" % (self._videosink, x, y, w, h)
            self._videosink.set_property ("rectangle", "%d,%d,%d,%d" % (x, y, w, h))

    def do_cleanup(self):
        current = self.player.get_state(0)[1]
        if current != gst.STATE_NULL:
            gst.info('do_cleanup')
            self.player.set_state(gst.STATE_READY)
            current = self.player.get_state(1000000000)[1]
            gst.info('changed state to %s' % current)
            self.player.set_state(gst.STATE_NULL)
            current = self.player.get_state(1000000000)[1]
            gst.info('changed state to %s' % current)
        self.playing = False
        self.on_eos = False
        self._cbuffering = -1
        self.status = self.STOPPED
        self.target_status = self.STOPPED
        self._dir = 1
        self._rate = 1.0

        self.c_video = 0
        self.c_audio = 0
        self.c_text = 0

        self.n_video = 0
        self.n_audio = 0
        self.n_text = 0

    def on_message_async_done(self, bus, message):
        if self.is_playbin2:
            self.n_video = self.player.get_property('n-video')
            self.n_audio = self.player.get_property('n-audio')
            self.n_text = self.player.get_property('n-text')
            print "%d video %d audio %d text" % (self.n_video, self.n_audio, self.n_text)
            if self.n_video > 0:
                self.player.set_property("current-video", self.c_video)
            if self.n_audio > 0:
                self.player.set_property("current-audio", self.c_audio)
            if self.n_text > 0:
                self.player.set_property("current-text", self.c_text)
            sink = self.player.get_property('video-sink')
            if sink is not None:
                factory = sink.get_factory()
                if "ismd_vidrend_bin" in factory.get_name():
                    print "SMD video sink %s" % sink
                    self._videosink = sink
                elif "autovideosink" in factory.get_name():
                    print "Found Autovideosink"
                    for child in sink.sinks():
                        factory = child.get_factory()
                        if "ismd_vidrend_bin" in factory.get_name():
                            print "SMD video sink %s" % child
                            self._videosink = child

            sink = self.player.get_property('audio-sink')
            if sink is not None:
                factory = sink.get_factory()
                if "ismd_audio_sink" in factory.get_name():
                    print "SMD audio sink %s" % sink
                elif "autoaudiosink" in factory.get_name():
                    print "Found Autoaudiosink"
                    for child in sink.sinks():
                        factory = child.get_factory()
                        if "ismd_audio_sink" in factory.get_name():
                            print "SMD audio sink %s" % child
        else:
            for child in self.player.sinks():
                factory = child.get_factory()
                if "ismd_vidrend_bin" in factory.get_name():
                    print "SMD video sink %s" % child
                    self._videosink = child
                elif "ismd_audio_sink" in factory.get_name():
                    print "SMD audio sink %s" % child
        
    def on_source_created(self, pipeline, *args):
        source = self.player.get_property('source')
        try:
            source.set_property("user-agent", "quicktime")
        except:
            pass

    def on_message_eos(self, bus, message):
        if self.on_eos:
            self.on_eos()
        self.playing = False

    def on_message_error(self, bus, message):
        err, msg = message.parse_error()
        code = message.structure['gerror'].code
        self.stop()
        self.status = self.STOPPED
        self.target_status = self.status
        gst.error("Gstreamer %s:%s" % (err, msg))

    def on_message_state_changed(self, bus, message):
        if message.src != self.player:
            return

        old_state, new_state, pending = message.parse_state_changed()
        gst.info ("old %s current %s pending %s status %s target status %s" % \
            (old_state, new_state, pending, self.status, self.target_status))
        if new_state == gst.STATE_PLAYING:
            if self.status != self.PLAYING:
                self.status = self.PLAYING
                self.playing = True

        elif new_state == gst.STATE_PAUSED:
            if self.status != self.BUFFERING:
                if self.target_status == self.PLAYING:
                    self.player.set_state(gst.STATE_PLAYING)
                else:
                    self.status = self.PAUSED
        elif new_state == gst.STATE_READY:
            if self._videosink is not None:
                del self._videosink
                self._videosink = None

    def on_message_buffering(self, bus, message):
        percent = message.parse_buffering()

        if math.floor(percent/5) > self._cbuffering:
            self._cbuffering = math.floor(percent/5)
            print "buffering %s" % percent

        if percent == 100:
            if self.target_status == self.PLAYING:
                gst.info("buffering done going to play")
                self.status = self.target_status
                self.player.set_state(gst.STATE_PLAYING)
            self._cbuffering = -1
        elif self.status != self.BUFFERING:
            if self.status == self.PLAYING:
                self.player.set_state(gst.STATE_PAUSED)

            self.status = self.BUFFERING

    def on_message_clock_lost(self, bus, message):
        self.player.set_state(gst.STATE_PAUSED)
        self.player.set_state(gst.STATE_PLAYING)

    def set_location(self, location):
        self.player.set_property('uri', location)

    def query_position(self):
        "Returns a (position, duration) tuple"
        try:
            position, format = self.player.query_position(gst.FORMAT_TIME)
        except:
            position = gst.CLOCK_TIME_NONE

        try:
            duration, format = self.player.query_duration(gst.FORMAT_TIME)
        except:
            duration = gst.CLOCK_TIME_NONE

        return (position, duration)

    def seek(self, location):
        """
        @param location: time to seek to, in nanoseconds
        """
        gst.debug("seeking to %r" % location)

        if self._rate < 0:
            res = self.player.seek (self._rate, gst.FORMAT_TIME,
                gst.SEEK_FLAG_FLUSH | gst.SEEK_FLAG_KEY_UNIT,
                gst.SEEK_TYPE_SET, 0,
                gst.SEEK_TYPE_SET, location)
        else:
            res = self.player.seek (self._rate, gst.FORMAT_TIME,
                gst.SEEK_FLAG_FLUSH | gst.SEEK_FLAG_KEY_UNIT,
                gst.SEEK_TYPE_SET, location,
                gst.SEEK_TYPE_SET, -1)

        if not res:
            gst.error("seek to %r failed" % location)

    def pause(self):
        gst.info("pausing player")
        self.target_status = self.PAUSED
        self.player.set_state(gst.STATE_PAUSED)
        current = self.player.get_state(0)[1]
        self.playing = False

    def play(self):
        gst.info("playing player")
        self.target_status = self.PLAYING
        current = self.player.get_state(0)[1]
        if current == gst.STATE_PAUSED:
            self.player.set_state(gst.STATE_PLAYING)
        elif current != gst.STATE_PLAYING :
            self.player.set_state(gst.STATE_PAUSED)
            self.status = self.PREROLLING

    def stop(self):
        self.player.set_state(gst.STATE_READY)
        current = self.player.get_state(0)[1]
        gst.info('changed state to %s' % current)
        if self.status != self.STOPPED:
            self.status = self.STOPPED
            self.target_status = self.status
        gst.info("stopped player")

    def get_state(self, timeout=1):
        return self.player.get_state(timeout=timeout)

    def is_playing(self):
        return self.playing

    def toggle_direction(self):
        self._dir = -self._dir
        self.set_rate (abs(self._rate))

    def set_rate(self, rate):
        self.pause()

        self._rate = self._dir * rate
        try:
            position, format = self.player.query_position(gst.FORMAT_TIME)
        except:
            position = gst.CLOCK_TIME_NONE

        if self._rate < 0:
            res = self.player.seek(self._rate, gst.FORMAT_TIME,
                gst.SEEK_FLAG_FLUSH,
                gst.SEEK_TYPE_SET, 0,
                gst.SEEK_TYPE_SET, position)
        else:
            res = self.player.seek(self._rate, gst.FORMAT_TIME,
                gst.SEEK_FLAG_FLUSH,
                gst.SEEK_TYPE_SET, position,
                gst.SEEK_TYPE_SET, -1)

        if res:
            gst.info("rate set to %s" % rate)
        else:
            gst.warning("change rate failed")
        self.play()

    def next_video_stream(self):
        if self.n_video > 0:
            current = (self.c_video + 1) % self.n_video
            if current != self.c_video:
                self.player.set_property("current-video", current)
                self.c_video = current

    def next_audio_stream(self):
        if self.n_audio > 0:
            current = (self.c_audio + 1) % self.n_audio
            if current != self.c_audio:
                self.player.set_property("current-audio", current)
                self.c_audio = current

    def next_text_stream(self):
        if self.n_text > 0:
            current = (self.c_text + 1) % self.n_text
            if current != self.c_text:
                self.player.set_property("current-text", current)
                self.c_text = current

    def add_PiP(self, uri):
        if self.PiPBin is not None:
            self.remove_PiP()
        self.PiPBin = PiPBin(uri, 5)
        self.player.add(self.PiPBin)
        self.PiPBin.sync_state_with_parent()

    def remove_PiP(self):
        if self.PiPBin is not None:
             self.PiPBin.set_state(gst.STATE_NULL)
             self.player.remove(self.PiPBin)
             del self.PiPBin
             self.PiPBin = None
    def change_plane(self):
        self._videosink.set_property('gdl-plane', 5)


class PlayerTUI():
    def __init__(self, args):

        self.duration = gst.CLOCK_TIME_NONE
        if not gst.uri_is_valid(args[1]):
            self.media_list = None
            self.media_idx = 0
            self.media_count = 0
            custom_pipeline = args[1]
        else:
            self.media_list = args
            self.media_idx = 1
            self.media_count = len(args)
            custom_pipeline = None

        self.player = GstPlayer(custom_pipeline)

        self.moving = False
        self.pos = 0
        self.seek_pos = 0
        self.stress_count = 0
        self.PiP = False
        self.paused = False

        def on_eos():
            self.player.stop()
            self.media_idx = self.media_idx + 1
            if self.media_idx < self.media_count:
                self.player.set_location(self.media_list[self.media_idx])
                self.player.play()
            else:
                self.quit()

        print "q: quit"
        print "l: show position"
        print "f: seek forward"
        print "b: seek backward"
        print "p: pause"
        print "s: stop"
        print "r: resume playback (play)"
        print "h: half speed"
        print "1: normal rate"
        print "2: 2x playback"
        print "4: 4x playback"
        print "8: 4x playback"
        print "R: toggle reverse playback"
        print "n: next clip"
        print "m: moving rectangle"
        print "M: moving rectangle"        
        print "V: iterate on video streams"
        print "A: iterate on audio streams"
        print "T: iterate on text streams"
        print "S: stress test for pipeline state changes"
        print "P: PiP sample (better with 2 uris in the input)"

        self.player.on_eos = lambda *x: on_eos()
        # The MainLoop
        self.mainloop = gobject.MainLoop()
        gobject.io_add_watch(sys.stdin, gobject.IO_IN, self.on_stdin)
        if not self.media_list is None:
            self.player.set_location(self.media_list[self.media_idx])
        self.player.play()
        try:
            self.mainloop.run()
        except KeyboardInterrupt:
            self.quit()

    def on_stdin(self, fd, condition):
        c = os.read(fd.fileno(), 1)

        if c == "q":
            self.quit()
        elif c == "l":
            self.show_position()
        elif c == "f":
            self.seek_forward()
        elif c == "b":
            self.seek_backward()
        elif c == "p":
            self.player.pause()
            self.paused = True
        elif c == "s":
            self.seek_pos = 0
            self.player.stop()
        elif c == "r":
            self.moving = False
            self.player.play()
            self.paused = False            
        elif c == "h":
            self.player.set_rate(0.5)
        elif c == "1":
            self.player.set_rate(1.0)
        elif c == "2":
            self.player.set_rate(2.0)
        elif c == "4":
            self.player.set_rate(4.0)
        elif c == "8":
            self.player.set_rate(8.0)
        elif c == "R":
            self.player.toggle_direction()
        elif c == "n":
            self.next_clip()
        elif c == "m":
            self.toggle_moving()
        elif c == "M":
            self.toggle_moving_resize()
        elif c == "V":
            self.player.next_video_stream()
        elif c == "A":
            self.player.next_audio_stream()
        elif c == "T":
            self.player.next_text_stream()
        elif c == "S":
            gobject.timeout_add (4000, self.stress_test,
                priority = gobject.PRIORITY_HIGH)
        elif c == "P":
            self.toggle_PiP()
        elif c == "G":
            self.change_plane()
        return True

    def move(self):
        if self.moving:
             self.pos = self.pos + 4
             if self.pos > 400:
                self.pos = 0
             self.player.update_rectangle (self.pos,self.pos,800,600)
             return True
        else:
             return False


    def move_resize(self):
        if self.moving:
             self.pos = self.pos + 4
             if self.pos > 400:
                self.pos = 0
             self.player.update_rectangle (self.pos,self.pos,800-self.pos,600-(self.pos/4))
             return True
        else:
             return False

    def toggle_PiP(self):
        self.player.pause()
        if self.PiP:
            self.player.remove_PiP()
            self.PiP = False
        else:
            media_idx = self.media_idx + 1
            if media_idx >= self.media_count:
                media_idx = 1

            self.player.add_PiP(self.media_list[media_idx])
            self.PiP = True
        self.player.play()

    def toggle_moving(self):
        if self.moving:
            self.moving = False
            self.player.update_rectangle (0,0,0,0)
        else:
            self.moving = True
            self.pos = 0
            self.player.update_rectangle (0,0,800,600)
            gobject.timeout_add (100, self.move,
                priority = gobject.PRIORITY_HIGH)

    def toggle_moving_resize(self):
        if self.moving:
            self.moving = False
            self.player.update_rectangle (0,0,0,0)
        else:
            self.moving = True
            self.pos = 0
            self.player.update_rectangle (0,0,800,600)
            gobject.timeout_add (100, self.move_resize,
                priority = gobject.PRIORITY_HIGH)

    def next_clip(self):
        self.seek_pos = 0
        self.player.stop()
        self.media_idx = self.media_idx + 1
        if self.media_idx < self.media_count:
            self.player.set_location(self.media_list[self.media_idx])
            self.player.play()
        else:
            self.media_idx = 1
            self.player.set_location(self.media_list[self.media_idx])
            self.player.play()

    def stress_test(self):
        self.stress_count = self.stress_count + 1
        print "Test #%d" % self.stress_count
        self.next_clip()
        return True

    def quit(self):
        self.player.stop()
        self.player.do_cleanup()
        gst.info("finished cleanup")
        del self.player
        self.player = None
        self.mainloop.quit()

    def show_position(self):
        position, duration = self.player.query_position()
        print "%s / %s" % (gst.TIME_ARGS(position), gst.TIME_ARGS(duration))

    def seek_forward(self):
        if not self.paused:
            self.player.pause()
        c_position, c_duration = self.player.query_position()
        if c_duration == gst.CLOCK_TIME_NONE:
            n_position = c_position + 5 * gst.SECOND
        else:
            if self.duration == gst.CLOCK_TIME_NONE:
                self.duration = c_duration
            self.seek_pos = min (self.seek_pos + 10, 90)
            n_position = self.duration * self.seek_pos / 100
        print "-> %s ( %d )" % (gst.TIME_ARGS(n_position), self.seek_pos)
        self.player.seek (n_position)
        if not self.paused:        
            self.player.play()

    def seek_backward(self):
        if not self.paused:
            self.player.pause()
        c_position, c_duration = self.player.query_position()
        if c_duration == gst.CLOCK_TIME_NONE:
            n_position = max (0,c_position - 5 * gst.SECOND)
        else:
            if self.duration == gst.CLOCK_TIME_NONE:
                self.duration = c_duration
            self.seek_pos = max (self.seek_pos - 10, 0)
            n_position = self.duration * self.seek_pos / 100
        print "-> %s ( %d )" % (gst.TIME_ARGS(n_position), self.seek_pos)
        self.player.seek (n_position)
        if not self.paused:        
            self.player.play()

def main(args):
    def usage():
        sys.stderr.write("usage: %s list of URI-OF-MEDIA-FILE\n" % args[0])
        sys.exit(1)

    if len(args) < 2:
        usage()

    tui = PlayerTUI(args)

if __name__ == '__main__':
    sys.exit(main(sys.argv))
