/*
    This file is provided under a dual BSD/LGPLv2.1 license.  When using
    or redistributing this file, you may do so under either license.

    LGPL LICENSE SUMMARY

    Copyright(c) 2008. Intel Corporation. All rights reserved.
    Copyright(c) 2009, 2010. Fluendo S.A. All rights reserved.

    This library is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2.1 of the
    License.

    This library is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
    USA. The full GNU Lesser General Public License is included in this
    distribution in the file called LICENSE.LGPL.

    Contact Information for Intel:
        Intel Corporation
        2200 Mission College Blvd.
        Santa Clara, CA  97052

    Contat Information for Fluendo:
        FLUENDO S.A.
        World Trade Center Ed Norte 4 pl.
        Moll de Barcelona
        08039 BARCELONA - SPAIN

    BSD LICENSE

    Copyright (c) 2008. Intel Corporation. All rights reserved.
    Copyright(c) 2009, 2010. Fluendo S.A. All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

      - Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      - Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in
        the documentation and/or other materials provided with the
        distribution.
      - Neither the name of Intel Corporation nor the names of its
        contributors may be used to endorse or promote products derived
        from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>

#include "vidtex.h"

//-----------------------------------------------------------------------------
// GDL SETUP
//-----------------------------------------------------------------------------

char
plane_id (gdl_plane_id_t plane)
{
  switch (plane) {
    case GDL_PLANE_ID_UPP_A:
      return 'A';
    case GDL_PLANE_ID_UPP_B:
      return 'B';
    case GDL_PLANE_ID_UPP_C:
      return 'C';
    case GDL_PLANE_ID_UPP_D:
      return 'D';
    default:
      return '?';
  }
}

void
setup_gdl (gdl_plane_id_t plane, int dstwidth, int dstheight)
{
  gdl_ret_t rc;
  gdl_pixel_format_t pix_fmt = GDL_PF_ARGB_32;
  gdl_color_space_t color_space = GDL_COLOR_SPACE_RGB;
  gdl_rectangle_t src_rect;
  gdl_rectangle_t dst_rect;
  gdl_rectangle_t tmp_rect;

  gdl_init (0);

  dst_rect.origin.x = 0;
  dst_rect.origin.y = 0;
  dst_rect.width = dstwidth;
  dst_rect.height = dstheight;

  src_rect.origin.x = 0;
  src_rect.origin.y = 0;
  src_rect.width = dstwidth;
  src_rect.height = dstheight;

  rc = gdl_plane_get_attr (plane, GDL_PLANE_DST_RECT, &tmp_rect);
  if (rc != GDL_SUCCESS) {
    printf ("gdl_plane_get_attr(GDL_PLANE_DST_RECT) failed!\n");
  } else {
    if (!gfx_forceplane) {
      // setup GDL with current plane settings
      dst_rect = tmp_rect;
      src_rect = tmp_rect;
    }
  }

  /*
     rc = gdl_plane_get_attr(plane, GDL_PLANE_SRC_RECT, &tmp_rect);
     if (rc != GDL_SUCCESS)
     printf("gdl_plane_get_attr(GDL_PLANE_SRC_RECT) failed!\n");
   */

  rc = gdl_plane_config_begin (plane);
  if (rc != GDL_SUCCESS) {
    printf ("gdl_plane_config_begin(%d) failed!\n", plane);
    goto the_end;
  }

  rc = gdl_plane_set_attr (GDL_PLANE_SRC_COLOR_SPACE, &color_space);
  if (rc != GDL_SUCCESS) {
    gdl_plane_config_end (GDL_TRUE);
    printf ("gdl_plane_set_attr(GDL_PLANE_SRC_COLOR_SPACE) failed!\n");
    goto the_end;
  }

  rc = gdl_plane_set_attr (GDL_PLANE_PIXEL_FORMAT, &pix_fmt);
  if (rc != GDL_SUCCESS) {
    gdl_plane_config_end (GDL_TRUE);
    printf ("gdl_plane_set_attr(GDL_PLANE_PIXEL_FORMAT) failed!\n");
    goto the_end;
  }

  rc = gdl_plane_set_attr (GDL_PLANE_DST_RECT, &dst_rect);
  if (rc != GDL_SUCCESS) {
    gdl_plane_config_end (GDL_TRUE);
    printf ("gdl_plane_set_attr(GDL_PLANE_DEST_RECT) failed!\n");
    goto the_end;
  }
  gfx_plane_width = dst_rect.width;
  gfx_plane_height = dst_rect.height;

  rc = gdl_plane_set_attr (GDL_PLANE_SRC_RECT, &src_rect);
  if (rc != GDL_SUCCESS) {
    gdl_plane_config_end (GDL_TRUE);
    printf ("gdl_plane_set_attr(GDL_PLANE_SRC_RECT) failed!\n");
    goto the_end;
  }

  rc = gdl_plane_config_end (GDL_FALSE);
  if (rc != GDL_SUCCESS) {
    gdl_plane_config_end (GDL_TRUE);
    printf ("gdl_plane_config_end failed!\n");
    goto the_end;
  }

the_end:
  return;
}

void
cleanup_gdl (void)
{
  printf ("gdl_close()\n");
  gdl_close ();
}


//-----------------------------------------------------------------------------
// EGL setup
//-----------------------------------------------------------------------------

EGLDisplay display = EGL_NO_DISPLAY;
EGLSurface surface = EGL_NO_SURFACE;
EGLContext context = EGL_NO_CONTEXT;
EGLConfig config = 0;

PFNEGLCREATEIMAGEKHRPROC eglCreateImageKHR = NULL;
PFNEGLDESTROYIMAGEKHRPROC eglDestroyImageKHR = NULL;

void
setup_egl (NativeWindowType window)
{
  // multisampling slows things down considerably
  EGLint configAttrs[] = {
    EGL_BUFFER_SIZE, EGL_DONT_CARE,
    //EGL_SAMPLE_BUFFERS,    1,   /* multisample is set */
    //EGL_SAMPLES,           4,   /* number of samples per pixel */
    EGL_DEPTH_SIZE, 16,
    EGL_RED_SIZE, 8,
    EGL_GREEN_SIZE, 8,
    EGL_BLUE_SIZE, 8,
    EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
    EGL_NONE
  };
  EGLint contextAttrs[] = {
    EGL_CONTEXT_CLIENT_VERSION, 2,      // Specifies to use OpenGL ES 2.0
    EGL_NONE
  };
  EGLint numconfigs = 0;
  EGLint configId = 0;
  EGLint major = 0, minor = 0;
  EGLBoolean ok = EGL_FALSE;

  display = eglGetDisplay ((NativeDisplayType) EGL_DEFAULT_DISPLAY);
  if (EGL_NO_DISPLAY == display) {
    printf ("eglGetDisplay returned EGL_NO_DISPLAY\n");
    ok = EGL_FALSE;
    goto the_end;
  }

  ok = eglInitialize (display, &major, &minor);
  if (!ok) {
    printf ("eglInitialize failed!\n");
    goto the_end;
  }
  printf ("EGL %d.%d initialized\n", major, minor);

  ok = eglBindAPI (EGL_OPENGL_ES_API);
  if (!ok) {
    printf ("eglBindAPI failed!\n");
    goto the_end;
  }

  ok = eglChooseConfig (display, configAttrs, &config, 1, &numconfigs);
  if (!ok) {
    printf ("eglChooseConfig failed!\n");
    printf (" (NumConfigs=%x)\n", numconfigs);
    goto the_end;
  }

  ok = eglGetConfigAttrib (display, config, EGL_CONFIG_ID, &configId);
  if (!ok) {
    printf ("eglGetConfigAttrib failed!\n");
    goto the_end;
  }

  surface = eglCreateWindowSurface (display, config, window, NULL);
  if (surface == EGL_NO_SURFACE) {
    printf ("eglCreateWindowSurface failed!\n");
    ok = EGL_FALSE;
    goto the_end;
  }

  context = eglCreateContext (display, config, NULL, contextAttrs);
  if (context == EGL_NO_CONTEXT) {
    printf ("eglCreateContext() failed!\n");
    ok = EGL_FALSE;
    goto the_end;
  }

  ok = eglMakeCurrent (display, surface, surface, context);
  if (!ok) {
    printf ("eglMakeCurrent failed!\n");
    goto the_end;
  }
  // get EGLImageKHR functions

  eglCreateImageKHR =
      (PFNEGLCREATEIMAGEKHRPROC) eglGetProcAddress ("eglCreateImageKHR");
  if (NULL == eglCreateImageKHR) {
    printf ("eglGetProcAddress didn't find eglCreateImageKHR\n");
    abort ();
  }

  eglDestroyImageKHR =
      (PFNEGLDESTROYIMAGEKHRPROC) eglGetProcAddress ("eglDestroyImageKHR");
  if (NULL == eglDestroyImageKHR) {
    printf ("eglGetProcAddress didn't find eglDestroyImageKHR\n");
    abort ();
  }


the_end:
  return;
}

void
stop_rendering (void)
{
  EGLBoolean ok = EGL_FALSE;

  ok = eglMakeCurrent (display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
  if (!ok) {
    printf ("eglMakeCurrent failed (%s)\n", egl_error_string (eglGetError ()));
  }

  ok = eglDestroyContext (display, context);
  if (!ok) {
    printf ("eglDestroyContext failed (%s)\n",
        egl_error_string (eglGetError ()));
  }

  ok = eglDestroySurface (display, surface);
  if (!ok) {
    printf ("eglDestroySurface failed (%s)\n",
        egl_error_string (eglGetError ()));
  }
}

void
cleanup_egl (void)
{
  EGLBoolean ok = eglTerminate (display);
  if (!ok) {
    printf ("eglTerminate failed (%s)\n", egl_error_string (eglGetError ()));
  }
}


//-----------------------------------------------------------------------------
// ERROR REPORTING FUNCTIONS RELATED TO GRAPHICS
//-----------------------------------------------------------------------------

// convenience macro for converting constants to strings
#define CASESTRING(var,x) case x: var = #x; break

char *
gdl_error_string (gdl_ret_t rc)
{
  char *retval = "(unknown)";
  switch (rc) {
      CASESTRING (retval, GDL_SUCCESS);
      CASESTRING (retval, GDL_ERR_INVAL);
      CASESTRING (retval, GDL_ERR_BUSY);
      CASESTRING (retval, GDL_ERR_DISPLAY);
      CASESTRING (retval, GDL_ERR_SURFACE);
      CASESTRING (retval, GDL_ERR_COMMAND);
      CASESTRING (retval, GDL_ERR_NULL_ARG);
      CASESTRING (retval, GDL_ERR_NO_MEMORY);
      CASESTRING (retval, GDL_ERR_FAILED);
      CASESTRING (retval, GDL_ERR_INTERNAL);
      CASESTRING (retval, GDL_ERR_NOT_IMPL);
      CASESTRING (retval, GDL_ERR_MAPPED);
      CASESTRING (retval, GDL_ERR_NO_INIT);
      CASESTRING (retval, GDL_ERR_NO_HW_SUPPORT);
      CASESTRING (retval, GDL_ERR_INVAL_PF);
      CASESTRING (retval, GDL_ERR_INVAL_RECT);
      CASESTRING (retval, GDL_ERR_ATTR_ID);
      CASESTRING (retval, GDL_ERR_ATTR_NO_SUPPORT);
      CASESTRING (retval, GDL_ERR_ATTR_READONLY);
      CASESTRING (retval, GDL_ERR_ATTR_VALUE);
      CASESTRING (retval, GDL_ERR_PLANE_CONFLICT);
      CASESTRING (retval, GDL_ERR_DISPLAY_CONFLICT);
      CASESTRING (retval, GDL_ERR_TIMEOUT);
      CASESTRING (retval, GDL_ERR_MISSING_BEGIN);
      CASESTRING (retval, GDL_ERR_PLANE_ID);
      CASESTRING (retval, GDL_ERR_INVAL_PTR);
      CASESTRING (retval, GDL_ERR_INVAL_HEAP);
      CASESTRING (retval, GDL_ERR_HEAP_IN_USE);
      CASESTRING (retval, GDL_ERR_INVAL_CALLBACK);
      CASESTRING (retval, GDL_ERR_SCALING_POLICY);
      CASESTRING (retval, GDL_ERR_INVAL_EVENT);
      CASESTRING (retval, GDL_ERR_INVAL_IOCTL);
      CASESTRING (retval, GDL_ERR_SCHED_IN_ATOMIC);
      CASESTRING (retval, GDL_ERR_MMAP);
      CASESTRING (retval, GDL_ERR_HDCP);
      CASESTRING (retval, GDL_ERR_CONFIG);
      CASESTRING (retval, GDL_ERR_HDMI_AUDIO_PLAYBACK);
      CASESTRING (retval, GDL_ERR_HDMI_AUDIO_BUFFER_FULL);
      CASESTRING (retval, GDL_ERR_PLANE_ORIGIN_ODD);
      CASESTRING (retval, GDL_ERR_PLANE_HEIGHT_ODD);
      CASESTRING (retval, GDL_ERR_HANDLE);
      CASESTRING (retval, GDL_ERR_TVMODE_UNDEFINED);
      CASESTRING (retval, GDL_ERR_PREMULT_CONFLICT);
      CASESTRING (retval, GDL_ERR_SUSPENDED);
    default:
      break;
  }
  return retval;
}

char *
gma_error_string (gma_ret_t rc)
{
  char *retval = "(unknown)";
  switch (rc) {
      CASESTRING (retval, GMA_SUCCESS);
      CASESTRING (retval, GMA_ERR_NULL_ARG);
      CASESTRING (retval, GMA_ERR_INVAL_FUNCS);
      CASESTRING (retval, GMA_ERR_INVAL_INFO);
      CASESTRING (retval, GMA_ERR_NO_MEMORY);
      CASESTRING (retval, GMA_ERR_FAILED);
    default:
      break;
  }
  return retval;
}

char *
egl_error_string (EGLint rc)
{
  char *retval = "(unknown)";
  switch (rc) {
      CASESTRING (retval, EGL_SUCCESS);
      CASESTRING (retval, EGL_NOT_INITIALIZED);
      CASESTRING (retval, EGL_BAD_ACCESS);
      CASESTRING (retval, EGL_BAD_ALLOC);
      CASESTRING (retval, EGL_BAD_ATTRIBUTE);
      CASESTRING (retval, EGL_BAD_CONFIG);
      CASESTRING (retval, EGL_BAD_CONTEXT);
      CASESTRING (retval, EGL_BAD_CURRENT_SURFACE);
      CASESTRING (retval, EGL_BAD_DISPLAY);
      CASESTRING (retval, EGL_BAD_MATCH);
      CASESTRING (retval, EGL_BAD_NATIVE_PIXMAP);
      CASESTRING (retval, EGL_BAD_NATIVE_WINDOW);
      CASESTRING (retval, EGL_BAD_PARAMETER);
      CASESTRING (retval, EGL_BAD_SURFACE);
      CASESTRING (retval, EGL_CONTEXT_LOST);
    default:
      break;
  }
  return retval;
}

char *
gl_error_string (unsigned int rc)
{
  char *retval = "(unknown)";
  switch (rc) {
      CASESTRING (retval, GL_NO_ERROR);
      CASESTRING (retval, GL_INVALID_ENUM);
      CASESTRING (retval, GL_INVALID_VALUE);
      CASESTRING (retval, GL_INVALID_OPERATION);
      CASESTRING (retval, GL_OUT_OF_MEMORY);
      CASESTRING (retval, GL_TRUE);
    default:
      break;
  }
  return retval;
}

char *
ismd_error_string (ismd_result_t rc)
{
  char *retval = "(unknown)";
  switch (rc) {
      CASESTRING (retval, ISMD_SUCCESS);
      CASESTRING (retval, ISMD_ERROR_FEATURE_NOT_IMPLEMENTED);
      CASESTRING (retval, ISMD_ERROR_FEATURE_NOT_SUPPORTED);
      CASESTRING (retval, ISMD_ERROR_INVALID_VERBOSITY_LEVEL);
      CASESTRING (retval, ISMD_ERROR_INVALID_PARAMETER);
      CASESTRING (retval, ISMD_ERROR_INVALID_HANDLE);
      CASESTRING (retval, ISMD_ERROR_NO_RESOURCES);
      CASESTRING (retval, ISMD_ERROR_INVALID_RESOURCE);
      CASESTRING (retval, ISMD_ERROR_INVALID_QUEUE_TYPE);
      CASESTRING (retval, ISMD_ERROR_NO_DATA_AVAILABLE);
      CASESTRING (retval, ISMD_ERROR_NO_SPACE_AVAILABLE);
      CASESTRING (retval, ISMD_ERROR_TIMEOUT);
      CASESTRING (retval, ISMD_ERROR_EVENT_BUSY);
      CASESTRING (retval, ISMD_ERROR_OBJECT_DELETED);
      CASESTRING (retval, ISMD_ERROR_ALREADY_INITIALIZED);
      CASESTRING (retval, ISMD_ERROR_IOCTL_FAILED);
      CASESTRING (retval, ISMD_ERROR_INVALID_BUFFER_TYPE);
      CASESTRING (retval, ISMD_ERROR_INVALID_FRAME_TYPE);
      CASESTRING (retval, ISMD_ERROR_QUEUE_BUSY);
      CASESTRING (retval, ISMD_ERROR_NOT_FOUND);
      CASESTRING (retval, ISMD_ERROR_OPERATION_FAILED);
      CASESTRING (retval, ISMD_ERROR_PORT_BUSY);
      CASESTRING (retval, ISMD_ERROR_NULL_POINTER);
      CASESTRING (retval, ISMD_ERROR_INVALID_REQUEST);
      CASESTRING (retval, ISMD_ERROR_OUT_OF_RANGE);
      CASESTRING (retval, ISMD_ERROR_NOT_DONE);
      CASESTRING (retval, ISMD_ERROR_UNSPECIFIED);
    default:
      break;
  }
  return retval;
}

int
gma_error (const char *file, const int line, const char *function, gma_ret_t ec,
    char *s)
{
  if (ec != GMA_SUCCESS) {
    if (s)
      printf ("### GMA ERROR in %s: %s (0x%x) [%s:%d]\n",
          s, gma_error_string (ec), ec, file, line);
    else
      printf ("### GMA ERROR in %s(): %s (0x%x) [%s:%d]\n",
          function, gma_error_string (ec), ec, file, line);
    return TRUE;
  } else {
    return FALSE;
  }
}

int
gdl_error (const char *file, const int line, const char *function, gdl_ret_t ec,
    char *s)
{
  if (ec != GDL_SUCCESS) {
    if (s)
      printf ("GDL ERROR in %s: %s (0x%x) [%s:%d]\n",
          s, gdl_error_string (ec), ec, file, line);
    else
      printf ("GDL ERROR in %s(): %s (0x%x) [%s:%d]\n",
          function, gdl_error_string (ec), ec, file, line);
    return TRUE;
  } else {
    return FALSE;
  }
}

int
egl_error (const char *file, const int line, const char *function, char *s)
{
  GLuint ec = eglGetError ();
  if (ec != EGL_SUCCESS) {
    if (s)
      printf ("EGL ERROR in %s: %s (0x%x) [%s:%d]\n",
          s, egl_error_string (ec), ec, file, line);
    else
      printf ("EGL ERROR in %s(): %s (0x%x) [%s:%d]\n",
          function, egl_error_string (ec), ec, file, line);
    return TRUE;
  } else {
    return FALSE;
  }
}


int
gl_error (const char *file, const int line, const char *function, char *s)
{
  GLuint ec = glGetError ();
  if (ec != GL_NO_ERROR) {
    if (s)
      printf ("GL ERROR in %s: %s (0x%x) [%s:%d]\n",
          s, gl_error_string (ec), ec, file, line);
    else
      printf ("GL ERROR in %s(): %s (0x%x) [%s:%d]\n",
          function, gl_error_string (ec), ec, file, line);
    return TRUE;
  } else {
    return FALSE;
  }
}
