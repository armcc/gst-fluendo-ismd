/*
    This file is provided under a dual BSD/LGPLv2.1 license.  When using
    or redistributing this file, you may do so under either license.

    LGPL LICENSE SUMMARY

    Copyright(c) 2008. Intel Corporation. All rights reserved.
    Copyright(c) 2009, 2010. Fluendo S.A. All rights reserved.

    This library is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2.1 of the
    License.

    This library is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
    USA. The full GNU Lesser General Public License is included in this
    distribution in the file called LICENSE.LGPL.

    Contact Information for Intel:
        Intel Corporation
        2200 Mission College Blvd.
        Santa Clara, CA  97052

    Contat Information for Fluendo:
        FLUENDO S.A.
        World Trade Center Ed Norte 4 pl.
        Moll de Barcelona
        08039 BARCELONA - SPAIN

    BSD LICENSE

    Copyright (c) 2008. Intel Corporation. All rights reserved.
    Copyright(c) 2009, 2010. Fluendo S.A. All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

      - Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      - Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in
        the documentation and/or other materials provided with the
        distribution.
      - Neither the name of Intel Corporation nor the names of its
        contributors may be used to endorse or promote products derived
        from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// this file implements an instance of a graphics model

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "vidtex.h"
#include "matrix.h"

// 6 rectangles, 2 triangles per rectangle, 3 vertices per triangle
#define MODEL_VERTICES    36

// 2 coordinates per vertex
#define MODEL_VERTEX_TEXCOORD (MODEL_VERTICES * 2)      // 2 per vertex
#define MODEL_VERTEX_COORDS   (MODEL_VERTICES * 3)      // 3 per vertex
#define MODEL_VERTEX_COLORS   (MODEL_VERTICES * 4)      // 4 per vertex


// OPTIONS: There are three options that can be enabled to demonstrate
// custom processing of video frames:

// Colorize the two trianges of the output as an example of simple processing.
// Also if the video is not muted this makes it easy to see the graphics part.
extern int colorize;

// Set the alpha channel for the video frame textures. If the video is not
// muted and the video plane is behind the graphics plane, it will be visible.
extern GLfloat alpha_value;


// Enable this to cause the plane to rotate.
extern int animate_graphics;


// length for gl error messages
#define MSG_LEN     1024

// the model size
#define MODEL_WIDTH  1280.0f
#define MODEL_HEIGHT  720.0f
#define MODEL_ASPECT ((GLfloat)MODEL_WIDTH / (GLfloat)MODEL_HEIGHT)
#define ZBASE  (2000)
#define ZNEAR  (175)
#define ZDEPTH (ZNEAR+ZBASE)
#define ZFAR   (ZNEAR+ZBASE*2)

// animation variables
static GLfloat rotation_x = 0.0f;
static GLfloat rotation_y = 0.0f;

// MATRICES
#define IDENTITY_MATRIX \
  { 1, 0, 0, 0, \
    0, 1, 0, 0, \
    0, 0, 1, 0, \
    0, 0, 0, 1 }
// all are initialized to identity matrix
static GLfloat projection[16] = IDENTITY_MATRIX;
static GLfloat modelview[16] = IDENTITY_MATRIX;
static GLfloat mvp[16] = IDENTITY_MATRIX;

// shader program variables
static GLuint vertexShaderId;
static GLuint fragmentShaderId;
static GLuint programId;

// shader variables
static int this_mvp_loc;


//---------------------------------------------------------------------------
// shader utility functions
//---------------------------------------------------------------------------

// read a shader files
// Returns: shader text in a memory buffer
//          NULL if there is an error
// Notes:   calling function is responsible to free returned memory
static char *
readShaderText (const char *fileName, int *len)
{
  char *pText = NULL;

  if (fileName != NULL) {
    FILE *pFile;
    printf ("Loading %s\n", fileName);

    pFile = fopen (fileName, "r");
    if (pFile == NULL) {
      fprintf (stderr, "File not found! '%s'\n", fileName);
    } else {
      fseek (pFile, 0, SEEK_END);
      *len = ftell (pFile);
      rewind (pFile);

      if (*len > 0) {
        pText = (char *) malloc (*len + 1);
        if (pText != NULL) {
          *len = (int) fread (pText, sizeof (char), *len, pFile);
          pText[*len] = '\0';
        }
      }
      fclose (pFile);
    }
  } else {
    fprintf (stderr, "readShaderText() error: file name can not be empty.\n");
  }

  return (pText);
}

// create a shader
// Returns: 0 on failure
static unsigned int
createGLShader (const char *fileName, int shaderType)
{
  GLuint shaderId = 0;          // vertex or fragment shader Id
  char pInfoLog[MSG_LEN + 1];   // error message
  int shaderStatus;             // shader's status
  int infoLogLength;            // shader error information
  int shaderTexLen = 0;         // shader text length
  const char *pShaderText = NULL;

  pShaderText = readShaderText (fileName, &shaderTexLen);
  if (pShaderText != NULL) {
    shaderId = glCreateShader (shaderType);
    GLERR (glCreateShader);

    glShaderSource (shaderId, 1, (const char **) &pShaderText, &shaderTexLen);
    GLERR (glShaderSource);

    glCompileShader (shaderId);
    GLERR (glCompileShader);

    free ((void *) pShaderText);

    glGetShaderiv (shaderId, GL_COMPILE_STATUS, &shaderStatus);
    GLERR (glGetShaderiv);

    if (shaderStatus != GL_TRUE) {
      fprintf (stderr, "Error: Failed to compile GL %s Shader\n",
          shaderType == GL_VERTEX_SHADER ? "vertex" : "fragment");

      glGetShaderInfoLog (shaderId, MSG_LEN, &infoLogLength, pInfoLog);
      if (infoLogLength > MSG_LEN)
        infoLogLength = MSG_LEN;
      pInfoLog[infoLogLength] = '\0';
      fprintf (stderr, "%s", pInfoLog);

    } else {
      printf ("Created shder program: %s\n", fileName);
    }
  }
  return (shaderId);
}

//---------------------------------------------------------------------------
// this function sets the values of the texture coord array
// the array is static but needs to be initialized at runtime
//---------------------------------------------------------------------------

static void
set_tex_coord_model (float *tc, float width, float height)
{
  static char template[MODEL_VERTEX_TEXCOORD] = {
    '0', 'H', 'W', 'H', '0', '0',
    'W', 'H', 'W', '0', '0', '0',
    '0', 'H', 'W', 'H', '0', '0',
    'W', 'H', 'W', '0', '0', '0',
    'W', '0', '0', '0', '0', 'H',
    '0', 'H', 'W', 'H', 'W', '0',
    '0', 'H', 'W', 'H', 'W', '0',
    'W', '0', '0', '0', '0', 'H',
    '0', 'H', 'W', 'H', '0', '0',
    'W', 'H', 'W', '0', '0', '0',
    'W', 'H', 'W', '0', '0', 'H',
    'W', '0', '0', '0', '0', 'H'
  };
  int i;
  for (i = 0; i < MODEL_VERTEX_TEXCOORD; i++) {
    if (template[i] == '0') {
      tc[i] = 0.0f;
    } else if (template[i] == 'W') {
      tc[i] = width;
    } else if (template[i] == 'H') {
      tc[i] = height;
    }
  }
}

#define INDEX_POSITION   0      // verts[]
#define INDEX_INPUTCOLOR 1      // colors[]
#define INDEX_ALPHAVALUE 2      // float

static GLfloat verts_model[MODEL_VERTEX_COORDS] = {
#define MODEL_LEFT   ( -MODEL_ASPECT/2 )
#define MODEL_RIGHT  (  MODEL_ASPECT/2 )
#define MODEL_BOTTOM ( -0.5 )
#define MODEL_TOP    (  0.5 )
#define MODEL_DEPTH  (  0.0 )

#define FACE_WIDTH  (MODEL_WIDTH/2)
#define FACE_HEIGHT (MODEL_HEIGHT/2)

  -FACE_WIDTH, -FACE_HEIGHT, FACE_WIDTH,
  FACE_WIDTH, -FACE_HEIGHT, FACE_WIDTH,
  -FACE_WIDTH, FACE_HEIGHT, FACE_WIDTH,

  FACE_WIDTH, -FACE_HEIGHT, FACE_WIDTH,
  FACE_WIDTH, FACE_HEIGHT, FACE_WIDTH,
  -FACE_WIDTH, FACE_HEIGHT, FACE_WIDTH,

  -FACE_WIDTH, FACE_HEIGHT, FACE_WIDTH,
  FACE_WIDTH, FACE_HEIGHT, FACE_WIDTH,
  -FACE_WIDTH, FACE_HEIGHT, -FACE_WIDTH,

  FACE_WIDTH, FACE_HEIGHT, FACE_WIDTH,
  FACE_WIDTH, FACE_HEIGHT, -FACE_WIDTH,
  -FACE_WIDTH, FACE_HEIGHT, -FACE_WIDTH,

  -FACE_WIDTH, FACE_HEIGHT, -FACE_WIDTH,
  FACE_WIDTH, FACE_HEIGHT, -FACE_WIDTH,
  FACE_WIDTH, -FACE_HEIGHT, -FACE_WIDTH,

  FACE_WIDTH, -FACE_HEIGHT, -FACE_WIDTH,
  -FACE_WIDTH, -FACE_HEIGHT, -FACE_WIDTH,
  -FACE_WIDTH, FACE_HEIGHT, -FACE_WIDTH,

  -FACE_WIDTH, -FACE_HEIGHT, -FACE_WIDTH,
  FACE_WIDTH, -FACE_HEIGHT, -FACE_WIDTH,
  FACE_WIDTH, -FACE_HEIGHT, FACE_WIDTH,

  FACE_WIDTH, -FACE_HEIGHT, FACE_WIDTH,
  -FACE_WIDTH, -FACE_HEIGHT, FACE_WIDTH,
  -FACE_WIDTH, -FACE_HEIGHT, -FACE_WIDTH,

  FACE_WIDTH, -FACE_HEIGHT, FACE_WIDTH,
  FACE_WIDTH, -FACE_HEIGHT, -FACE_WIDTH,
  FACE_WIDTH, FACE_HEIGHT, FACE_WIDTH,

  FACE_WIDTH, -FACE_HEIGHT, -FACE_WIDTH,
  FACE_WIDTH, FACE_HEIGHT, -FACE_WIDTH,
  FACE_WIDTH, FACE_HEIGHT, FACE_WIDTH,

  -FACE_WIDTH, -FACE_HEIGHT, FACE_WIDTH,
  -FACE_WIDTH, FACE_HEIGHT, FACE_WIDTH,
  -FACE_WIDTH, -FACE_HEIGHT, -FACE_WIDTH,

  -FACE_WIDTH, FACE_HEIGHT, FACE_WIDTH,
  -FACE_WIDTH, FACE_HEIGHT, -FACE_WIDTH,
  -FACE_WIDTH, -FACE_HEIGHT, -FACE_WIDTH,
};

static GLfloat colors_model[MODEL_VERTEX_COLORS] = {
  // vertex colors
  // R, G, B, A
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,

  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,

  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,

  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,

  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,

  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 1.0f, 1.0f
};

static GLfloat colors_model2[MODEL_VERTEX_COLORS] = {
  // vertex colors
  // R, G, B, A
  1.0f, 0.2f, 0.5f, 1.0f,
  1.0f, 0.2f, 0.5f, 1.0f,
  1.0f, 0.2f, 0.5f, 1.0f,
  0.2f, 1.0f, 0.5f, 1.0f,
  0.2f, 1.0f, 0.5f, 1.0f,
  0.2f, 1.0f, 0.5f, 1.0f,

  1.0f, 0.2f, 0.5f, 1.0f,
  1.0f, 0.2f, 0.5f, 1.0f,
  1.0f, 0.2f, 0.5f, 1.0f,
  0.2f, 1.0f, 0.5f, 1.0f,
  0.2f, 1.0f, 0.5f, 1.0f,
  0.2f, 1.0f, 0.5f, 1.0f,

  1.0f, 0.2f, 0.5f, 1.0f,
  1.0f, 0.2f, 0.5f, 1.0f,
  1.0f, 0.2f, 0.5f, 1.0f,
  0.2f, 1.0f, 0.5f, 1.0f,
  0.2f, 1.0f, 0.5f, 1.0f,
  0.2f, 1.0f, 0.5f, 1.0f,

  1.0f, 0.2f, 0.5f, 1.0f,
  1.0f, 0.2f, 0.5f, 1.0f,
  1.0f, 0.2f, 0.5f, 1.0f,
  0.2f, 1.0f, 0.5f, 1.0f,
  0.2f, 1.0f, 0.5f, 1.0f,
  0.2f, 1.0f, 0.5f, 1.0f,

  1.0f, 0.2f, 0.5f, 1.0f,
  1.0f, 0.2f, 0.5f, 1.0f,
  1.0f, 0.2f, 0.5f, 1.0f,
  0.2f, 1.0f, 0.5f, 1.0f,
  0.2f, 1.0f, 0.5f, 1.0f,
  0.2f, 1.0f, 0.5f, 1.0f,

  1.0f, 0.2f, 0.5f, 1.0f,
  1.0f, 0.2f, 0.5f, 1.0f,
  1.0f, 0.2f, 0.5f, 1.0f,
  0.2f, 1.0f, 0.5f, 1.0f,
  0.2f, 1.0f, 0.5f, 1.0f,
  0.2f, 1.0f, 0.5f, 1.0f,
};

//---------------------------------------------------------------------------
// setup GL
// Initializes the vertex positions, colors and texture coordinates.
// Initializes OpenGL by setting up projection matrix, enabling necessary state 
// to pass positions, colors and texture coordinates to OpenGL. Get index for
// uniform variables used in vertex and fragment shaders.
//
// Returns true if successful, false if there are errors.
//---------------------------------------------------------------------------
static int
setup_model_gl ()
{
  int shaderStatus = GL_FALSE;  // shader status 
  int infoLen;                  // error checking info length
  char infoLog[MSG_LEN + 1];    // error checking info log
  int ret = true;               // set return value to be true 

  GLfloat w4 =
      ((GLfloat) src_texture_width - 0.5f) / (GLfloat) src_texture_stride;
  GLfloat h4 = 1.0;             // height / bufheight
  static GLfloat texCoord0[MODEL_VERTEX_TEXCOORD];
  static GLfloat texCoord1[MODEL_VERTEX_TEXCOORD];

  // report version
  fprintf (stdout, "OpenGL version is : %s\n", glGetString (GL_VERSION));
  GLERR (glGetString);

  // texture coordintes are 0.0 to 1.0
  // width is the ratio of pixels / stride
  set_tex_coord_model (texCoord0, w4, h4);      // shader
  set_tex_coord_model (texCoord1, w4, h4);      // shader

  // Create the Shader programs    
  vertexShaderId = createGLShader ("vertshader.txt", GL_VERTEX_SHADER);
  GLERR (NULL);

  fragmentShaderId = createGLShader ("fragshader.txt", GL_FRAGMENT_SHADER);
  GLERR (NULL);

  if (vertexShaderId && fragmentShaderId) {
    // Create Programs
    programId = glCreateProgram ();
    GLERR (NULL);

    // attach shaders to program
    glAttachShader (programId, vertexShaderId);
    GLERR (NULL);
    glAttachShader (programId, fragmentShaderId);
    GLERR (NULL);

    // name position and inputcolor indices
    glBindAttribLocation (programId, INDEX_POSITION, "position");
    GLERR (NULL);
    glBindAttribLocation (programId, INDEX_INPUTCOLOR, "inputcolor");
    GLERR (NULL);

    // attempt to link & validate
    glLinkProgram (programId);
    GLERR (glLinkProgram);

    glGetProgramiv (programId, GL_LINK_STATUS, &shaderStatus);
    GLERR (NULL);
    if (shaderStatus != GL_TRUE) {
      fprintf (stderr, "Error: Failed to link GLSL program\n");
      glGetProgramInfoLog (programId, MSG_LEN, &infoLen, infoLog);
      if (infoLen > MSG_LEN)
        infoLog[MSG_LEN] = '\0';
      fprintf (stderr, "%s\n", infoLog);
      ret = false;
    } else {
      glValidateProgram (programId);
      GLERR (NULL);
      glGetProgramiv (programId, GL_VALIDATE_STATUS, &shaderStatus);
      GLERR (NULL);
      if (shaderStatus != GL_TRUE) {
        fprintf (stderr, "Error: Failed to validate GLSL program\n");
        glGetProgramInfoLog (programId, MSG_LEN, &infoLen, infoLog);
        if (infoLen > MSG_LEN)
          infoLog[MSG_LEN] = '\0';
        fprintf (stderr, "%s\n", infoLog);
        ret = false;
      }
    }


    // pass arguments to shader programs
    if (shaderStatus == GL_TRUE) {
      glUseProgram (programId);
      fprintf (stdout, "Vertex and fragment shaders created. \n");

      // create mvp variable for vertex shader (see model_render)
      this_mvp_loc = glGetUniformLocation (programId, "mvp");
      GLERR (NULL);

      // associate textures with texture unit GL_TEXTURE0/GL_TEXTURE1
      int texture_posy = glGetUniformLocation (programId, "y_texture");
      GLERR (glGetUniformLocation);
      glUniform1i (texture_posy, 0);    // GL_TEXTURE0
      GLERR (glUniform1i);

      int texture_posc = glGetUniformLocation (programId, "c_texture");
      GLERR (glGetUniformLocation);
      glUniform1i (texture_posc, 1);    // GL_TEXTURE1
      GLERR (glUniform1i);

      // depth test is needed for 3D cube
      glEnable (GL_DEPTH_TEST);
      GLERR (NULL);

      int alphavalue_pos = glGetUniformLocation (programId, "alpha_value");
      GLERR (NULL);
      printf ("Setting alpha_value to %.2fF\n", alpha_value);
      glUniform1f (alphavalue_pos, alpha_value);
      GLERR (NULL);

      //----------------------------------------------------
      // setup the attribute arrays for verts[] and colors[]
      //----------------------------------------------------
      glVertexAttribPointer (INDEX_POSITION, 3, GL_FLOAT, 0, 0, verts_model);
      GLERR (NULL);
      glEnableVertexAttribArray (INDEX_POSITION);
      GLERR (NULL);

      if (colorize) {
        glVertexAttribPointer (INDEX_INPUTCOLOR, 4, GL_FLOAT, 0, 0,
            colors_model2);
      } else {
        glVertexAttribPointer (INDEX_INPUTCOLOR, 4, GL_FLOAT, 0, 0,
            colors_model);
      }
      GLERR (NULL);
      glEnableVertexAttribArray (INDEX_INPUTCOLOR);
      GLERR (NULL);

      //----------------------------------------------------
      // set the texture coordinates
      //----------------------------------------------------
      {
        int tex_loc0 = glGetAttribLocation (programId, "inputtexture0");
        GLERR (NULL);

        printf ("using texCoord0 wh=%f %f\n", w4, h4);
        glVertexAttribPointer (tex_loc0, 2, GL_FLOAT, 0, 0, texCoord0);
        GLERR (NULL);
        glEnableVertexAttribArray (tex_loc0);
        GLERR (NULL);

        int tex_loc1 = glGetAttribLocation (programId, "inputtexture1");
        GLERR (NULL);

        printf ("using texCoord1 wh=%f %f\n", w4, h4);
        glVertexAttribPointer (tex_loc1, 2, GL_FLOAT, 0, 0, texCoord1);
        GLERR (NULL);
        glEnableVertexAttribArray (tex_loc1);
        GLERR (NULL);
      }

      // setup projection for model_render
      myIdentity (projection);
      GLERR (myIdentity);

      myFrustum (projection, -100 * MODEL_ASPECT, 100 * MODEL_ASPECT, -100, 100,
          ZNEAR, ZFAR);
      if (GLERR (myFrustum)) {
        ret = false;
      }
      // compute mvp
      // only need to do this once unless animating
      myIdentity (modelview);
      myMultMatrix (mvp, projection, modelview);
      glUniformMatrix4fv (this_mvp_loc, 1, GL_FALSE, mvp);
    }
  } else {
    fprintf (stderr, "Error: Could not set up openGL properly\n");
    ret = false;
  }

  return (ret);
}

static void
close_model_gl ()
{
  if (vertexShaderId) {
    glDetachShader (programId, vertexShaderId);
  }
  if (fragmentShaderId) {
    glDetachShader (programId, fragmentShaderId);
  }
  if (programId) {
    glDeleteProgram (programId);
  }
  if (vertexShaderId) {
    glDeleteShader (vertexShaderId);
  }
  if (fragmentShaderId) {
    glDeleteShader (fragmentShaderId);
  }
}



//---------------------------------------------------------------------------
// Renders the model
//---------------------------------------------------------------------------
void
cube_render (void)
{
  if (animate_graphics) {
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // if animating, update the mvp matrix
    myIdentity (modelview);
    myTranslate (modelview, 0, 0, -ZDEPTH);
    myRotate (modelview, rotation_x, 1, 0, 0);
    myRotate (modelview, rotation_y, 0, 1, 0);

    myMultMatrix (mvp, projection, modelview);
    glUniformMatrix4fv (this_mvp_loc, 1, GL_FALSE, mvp);
  }
  glDrawArrays (GL_TRIANGLES, 0, MODEL_VERTICES);
}

//---------------------------------------------------------------------------
// rotate the model
//---------------------------------------------------------------------------
void
cube_update (void)
{
  // rotation constants
  static float ROTATE_X = 0.1f;
  static float ROTATE_Y = 0.5f;

  // rotate up and down within a narrow range
  rotation_x += ROTATE_X;
  if (rotation_x > 20.0f)
    ROTATE_X = -ROTATE_X;
  else if (rotation_x < -20.0f)
    ROTATE_X = -ROTATE_X;

  // rotate continously
  rotation_y += ROTATE_Y;
  if (rotation_y > 360.0f)
    rotation_y = 0.0f;
}

//---------------------------------------------------------------------------
// initialize the model
//---------------------------------------------------------------------------
void
cube_init (void)
{
  setup_model_gl ();
  printf ("ASPECT=%f\n", MODEL_ASPECT);
  printf ("gfx_plane_width    = %4d\n", gfx_plane_width);
  printf ("gfx_plane_height   = %4d\n", gfx_plane_height);
  printf ("src_texture_width  = %4d\n", src_texture_width);
  printf ("src_texture_height = %4d\n", src_texture_height);
}

//---------------------------------------------------------------------------
// cleanup the model
//---------------------------------------------------------------------------
void
cube_fini (void)
{
  close_model_gl ();
}
