/*
    This file is provided under a dual BSD/LGPLv2.1 license.  When using
    or redistributing this file, you may do so under either license.

    LGPL LICENSE SUMMARY

    Copyright(c) 2008. Intel Corporation. All rights reserved.
    Copyright(c) 2009, 2010. Fluendo S.A. All rights reserved.

    This library is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2.1 of the
    License.

    This library is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
    USA. The full GNU Lesser General Public License is included in this
    distribution in the file called LICENSE.LGPL.

    Contact Information for Intel:
        Intel Corporation
        2200 Mission College Blvd.
        Santa Clara, CA  97052

    Contat Information for Fluendo:
        FLUENDO S.A.
        World Trade Center Ed Norte 4 pl.
        Moll de Barcelona
        08039 BARCELONA - SPAIN

    BSD LICENSE

    Copyright (c) 2008. Intel Corporation. All rights reserved.
    Copyright(c) 2009, 2010. Fluendo S.A. All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

      - Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      - Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in
        the documentation and/or other materials provided with the
        distribution.
      - Neither the name of Intel Corporation nor the names of its
        contributors may be used to endorse or promote products derived
        from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __VIDTEX_H__
#define __VIDTEX_H__

#include <gst/gst.h>
#include <gst/app/gstappsink.h>

#include <libgdl.h>
#include <libgma.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <ismd_core.h>

//-----------------------------------------------------------------------------
// error reporting
//-----------------------------------------------------------------------------

// These macros follow a function call of the appropriate type
// and report any error. The caller can provide either a NULL string,
// or arguments for printf(). The GDL macro requires the return code.
#define GDLERR(rc, s...) gdl_error(__FILE__, __LINE__, __FUNCTION__, rc, #s)
#define GMAERR(rc, s...) gma_error(__FILE__, __LINE__, __FUNCTION__, rc, #s)
#define EGLERR(s...) egl_error(__FILE__, __LINE__, __FUNCTION__, #s)
#define GLERR(s...)  gl_error(__FILE__, __LINE__, __FUNCTION__, #s)

// These functions format error messages, and are meant to be used
// with the above macros
int gdl_error (const char *file, const int line, const char *function,
    gdl_ret_t rc, char *s);
int gma_error (const char *file, const int line, const char *function,
    gma_ret_t rc, char *s);
int egl_error (const char *file, const int line, const char *function, char *s);
int gl_error (const char *file, const int line, const char *function, char *s);

// these functions convert an error code into a string, or return
// "(unknown)" for an unrecognized error code.
char *gdl_error_string (gdl_ret_t rc);
char *gma_error_string (gma_ret_t rc);
char *egl_error_string (EGLint rc);
char *gl_error_string (unsigned int rc);
char *ismd_error_string (ismd_result_t rc);

//-----------------------------------------------------------------------------
// global variables
//-----------------------------------------------------------------------------

// EGL variables
extern EGLDisplay display;
extern EGLSurface surface;

// Force the graphics gdl plane src and dest to specified width and height.
// Values are 'true' or 'false'.
// If false, defaults to whatever the current plane configuration is.
extern int gfx_forceplane;

// The size of the graphics plane dest rect. Set by setup_gdl()
extern gdl_plane_id_t gfx_plane;
extern int gfx_plane_width;
extern int gfx_plane_height;

// The size and stride of the image buffer returned from SMD vidrend.
extern int src_texture_width;
extern int src_texture_height;
extern const int src_texture_stride;

// Scaling factor for graphics. This comes from main.c and is a
// number from 0.0 to 1.0 to scale the graphics relative to the
// overall plane size. 1.0 is full screen, 0.5 is half screen, etc.
extern float gfxscale;

//-----------------------------------------------------------------------------
// Player related declarations
//-----------------------------------------------------------------------------

typedef struct _Player Player;

struct _Player
{
  GstElement *pipeline;
  GstElement *sink;
  volatile gint on_eos;
  volatile gint on_buffer;
};

#define PLAYER_IS_EOS(s)         (g_atomic_int_get(&(s)->on_eos))
#define PLAYER_SET_EOS(s,val)    (g_atomic_int_set(&(s)->on_eos, (val)))
#define PLAYER_IS_BUFFER(s)      (g_atomic_int_get(&(s)->on_buffer))
#define PLAYER_SET_BUFFER(s,val) (g_atomic_int_set(&(s)->on_buffer, (val)))

Player *player_new (char *uri, int width, int height);
void player_free (Player * player);

void player_play (Player * player);
void player_pause (Player * player);
void player_stop (Player * player);

//-----------------------------------------------------------------------------
// external function declarations
//-----------------------------------------------------------------------------

// setup and tear down GDL
void setup_gdl (gdl_plane_id_t plane, int texwidth, int texheight);
void cleanup_gdl (void);

// convert a gdl plane id to a char representation  (i.e., 'A')
char plane_id (gdl_plane_id_t plane);

// setup and tear down EGL
void setup_egl (NativeWindowType window);       // window = GDL plane
void stop_rendering (void);
void cleanup_egl (void);

// pointers to ImageKHR functions. Initialized by setup_egl()
extern PFNEGLCREATEIMAGEKHRPROC eglCreateImageKHR;
extern PFNEGLDESTROYIMAGEKHRPROC eglDestroyImageKHR;
extern PFNGLEGLIMAGETARGETTEXTURE2DOESPROC glEGLImageTargetTexture2DOES;

// initialize graphics. This can be done before the vidrend handle
// is known. If gfx_forceplane is true, sets the plane to width/height.
void graphics_init (gdl_plane_id_t plane, int width, int height);

// tries to get the next video buffer from SMD. If nothing is available
// returns false. If a buffer is available, it is processed and rendered
// and this function returns true.
int graphics_update (Player * player);

// cleans everything up
void graphics_fini (void);

// creates the geometry at startup
void model_init (void);

// called to draw the geometry. The caller should have already set up
// the active textures for texture units 1 and 2 prior to calling
void model_render (void);

// performs any geometry related cleanup at shutdown
void model_fini (void);

void cube_render (void);
void cube_update (void);
void cube_init (void);
void cube_fini (void);

#endif
