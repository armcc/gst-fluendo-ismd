#!/usr/bin/env python
# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

# -----------------------------------------------------------------------------
# 
#    This file is provided under a dual BSD/LGPLv2.1 license.  When using
#    or redistributing this file, you may do so under either license.
#
#    LGPL LICENSE SUMMARY
#
#    Copyright(c) 2009, 2011. Fluendo S.A. All rights reserved.
#
#    This library is free software; you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation; either version 2.1 of the
#    License.
#
#    This library is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
#    USA. The full GNU Lesser General Public License is included in this
#    distribution in the file called LICENSE.LGPL.
#
#    Contat Information for Fluendo:
#        FLUENDO S.A.
#        World Trade Center Ed Norte 4 pl.
#        Moll de Barcelona
#        08039 BARCELONA - SPAIN
#
#    BSD LICENSE
#
#    Copyright(c) 2009, 2011. Fluendo S.A. All rights reserved.
#
#    Redistribution and use in source and binary forms, with or without
#    modification, are permitted provided that the following conditions
#    are met:
#
#      - Redistributions of source code must retain the above copyright
#        notice, this list of conditions and the following disclaimer.
#      - Redistributions in binary form must reproduce the above copyright
#        notice, this list of conditions and the following disclaimer in
#        the documentation and/or other materials provided with the
#        distribution.
#      - Neither the name of Intel Corporation nor the names of its
#        contributors may be used to endorse or promote products derived
#        from this software without specific prior written permission.
#
#    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import sys, os

import gobject
gobject.threads_init()

# import pygst
# pygst.require('0.10')
import gst
import math
       
class CustomPipeline(gst.Pipeline):

    def __init__(self):
        gst.Pipeline.__init__(self, 'CustomPipeline')

        self._src = None
        self._videobin = None
        self._audiobin = None

        self._vlinked = False
        self._alinked = False

        self._enable_buffering = True
        self._buffering_size = 512 * 1024        
        self._underrun_signal = None
        self._running_signal = None


    def link_videopad(self, srcpad):
        if not self._vlinked:
            print 'link video pad to %s' % srcpad
            sinkpad = self._vdec.get_pad('sink')
            srcpad.link (sinkpad)
            self._vlinked = True

    def link_audiopad(self, srcpad):
        if not self._alinked:
            print 'link audio pad to %s' % srcpad
            sinkpad = self._asink.get_pad('sink')
            srcpad.link (sinkpad)
            self._alinked = True

    def on_underrun (self, queue):
            print "underrun detected"
            if self._running_signal is None:
                self._queue.set_property ("min-threshold-bytes", self._buffering_size)
                self._running_signal = self._queue.connect('running', self.on_running)
            self.post_message (gst.message_new_buffering (self, 0))

    def on_running (self, queue):
            print "pipeline prerolled"
            self._queue.set_property ("min-threshold-bytes", 0)
            self._queue.disconnect (self._running_signal)
            self._running_signal = None
            self.post_message (gst.message_new_buffering (self, 100))
                
    def build_pipeline (self, program):
        if self._src is None:
            print "Opening %s" % program
            gst.debug('create dvbsrc')
            self._src = gst.parse_launch('ismd_dvb_src program=%s' % program)
            #self._src.set_property ("clock-recovery", False)
            gst.debug('adding dvbsrc %s to bin' % self._src)
            self.add(self._src)
            gst.debug('create queue')
            self._queue = gst.element_factory_make('queue', 'tsqueue')
            if self._enable_buffering:
                self._queue.set_property ("min-threshold-bytes", self._buffering_size)
                self._queue.set_property ("max-size-buffers", 0)
                self._underrun_signal = self._queue.connect('underrun', self.on_underrun)
                self._running_signal = self._queue.connect('running', self.on_running)
            self.add(self._queue)

            self._demux = gst.element_factory_make('flutsdemux')
            self._demux.set_property ("max-pes-buffer-size", 8192)
            self.add(self._demux)

            # Connect handler for 'pad-added' signal
            self._demux.connect('pad-added', self.on_new_decoded_pad)

            gst.element_link_many(self._src, self._queue, self._demux)

            self._vdec = gst.element_factory_make('ismd_mpeg2_viddec')
            self._vdec.set_property ("use-buffering", False)
            self.add(self._vdec)

            self._vsink = gst.element_factory_make('ismd_vidrend_bin')
            self.add(self._vsink)

            gst.element_link_many(self._vdec, self._vsink)

            self._asink = gst.element_factory_make('ismd_audio_sink')
            self._asink.set_property ("use-buffering", False)
            self.add(self._asink)

            self._asink.sync_state_with_parent()            
            self._vsink.sync_state_with_parent()
            self._vdec.sync_state_with_parent()
            self._demux.sync_state_with_parent()
            self._queue.sync_state_with_parent()
            self._src.sync_state_with_parent()
            
    def destroy_pipeline(self):
        if self._src is not None:
            gst.info("removing src elements")
            if self._underrun_signal is not None:
                self._queue.disconnect (self._underrun_signal)

            if self._running_signal is not None:
                self._queue.disconnect(self._running_signal)

            gst.element_unlink_many(self._src, self._queue, self._demux, self._vdec, self._vsink, self._asink)
            self._vlinked = False
            self._alinked = False
            
            self.remove (self._src, self._queue, self._demux, self._vdec, self._vsink, self._asink)
            del self._asink
            del self._vsink
            del self._vdec
            del self._demux
            del self._queue
            del self._src
            self._asink = None
            self._vsink = None
            self._vdec = None
            self._demux = None
            self._queue = None
            self._src = None

    def do_cleanup(self):
        current = self.get_state(0)[1]
        if current != gst.STATE_NULL:
            gst.info('do_cleanup')
            self.set_state(gst.STATE_READY)
            current = self.get_state(0)[1]
            gst.info('changed state to %s' % current)
            self.set_state(gst.STATE_NULL)
            current = self.get_state(0)[1]
            self.destroy_pipeline()
            gst.info('changed state to %s' % current)

    def open_program(self, program):
        current = self.get_state(0)[1]
        self.do_cleanup()
        self.build_pipeline (program)

        gst.info('going to change state to %s' % current)
        self.set_state(current)
   
    def on_new_decoded_pad(self, element, pad):
        padname = pad.get_name()
        caps = pad.get_caps()
        name = caps[0].get_name()
        gst.info('padname %s %s' % (padname, caps))
        if 'video/' in name:
            self.link_videopad(pad)
        elif 'audio/' in name:
            self.link_audiopad(pad)

class GstPlayer:
    STOPPED = 0
    PLAYING = 1
    PAUSED = 2
    BUFFERING = 3
    PREROLLING = 4

    def __init__(self):
        self.playing = False
        self.on_eos = False
        self.on_error = False
        self._cbuffering = -1
        self.status = self.STOPPED
        self.target_status = self.STOPPED
        self._rate = 1.0

        self.player = CustomPipeline()

        bus = self.player.get_bus()
        # bus.enable_sync_message_emission()
        # bus.connect('message', self.on_message)
        bus.connect('message::eos', self.on_message_eos)
        bus.connect('message::error', self.on_message_error)
        bus.connect('message::state-changed', self.on_message_state_changed)
        bus.connect('message::buffering', self.on_message_buffering)
        bus.connect('message::clock-lost', self.on_message_clock_lost)
        bus.add_signal_watch()

    def do_cleanup(self):
        if self.player is not None:
            self.player.do_cleanup()

    def on_message(self, bus, message):
        t = message.type
        if t == gst.MESSAGE_ERROR:
            err, debug = message.parse_error()
            gst.info("Error: %s" % err, debug)
            if self.on_eos:
                self.on_eos()
            self.playing = False
        elif t == gst.MESSAGE_EOS:
            if self.on_eos:
                self.on_eos()
            self.playing = False
        elif t == gst.MESSAGE_NEW_CLOCK:
            gst.info("new clock")
            self.player.set_state(gst.STATE_PAUSED)
            self.player.set_state(gst.STATE_PLAYING)
        else:
            gst.info("received message: %s" % message)

    def on_message_eos(self, bus, message):
        if self.on_eos:
            self.on_eos()
        self.playing = False

    def on_message_error(self, bus, message):
        err, msg = message.parse_error()
        code = message.structure['gerror'].code
        print "Gstreamer %s:%s" % (err, msg)
        if self.on_error:
            self.on_error()

    def on_message_state_changed(self, bus, message):
        if message.src != self.player:
            return

        old_state, new_state, pending = message.parse_state_changed()
        gst.info ("old %s current %s pending %s status %s target status %s" % \
            (old_state, new_state, pending, self.status, self.target_status))
        if new_state == gst.STATE_PLAYING:
            if self.status != self.PLAYING:
                self.status = self.PLAYING
                self.playing = True

        elif new_state == gst.STATE_PAUSED:
            if self.status != self.BUFFERING:
                if self.target_status == self.PLAYING:
                    self.player.set_state(gst.STATE_PLAYING)
                else:
                    self.status = self.PAUSED

    def on_message_buffering(self, bus, message):
        percent = message.parse_buffering()

        if math.floor(percent/5) > self._cbuffering:
            self._cbuffering = math.floor(percent/5)
            print "buffering %s" % percent
            gst.info ("buffering %s" % percent)

        if percent == 100:
            if self.target_status == self.PLAYING:
                gst.info("buffering done going to play")
                self.status = self.target_status
                self.player.set_state(gst.STATE_PLAYING)
            self._cbuffering = -1
        elif self.status != self.BUFFERING:
            if self.status == self.PLAYING:
                self.player.set_state(gst.STATE_PAUSED)

            self.status = self.BUFFERING

    def on_message_clock_lost(self, bus, message):
        self.player.set_state(gst.STATE_PAUSED)
        self.player.set_state(gst.STATE_PLAYING)

    def open_program(self, program):
        self.player.open_program(program)

    def pause(self):
        gst.info("pausing player")
        self.target_status = self.PAUSED
        self.player.set_state(gst.STATE_PAUSED)
        self.playing = False

    def play(self):
        gst.info("playing player")
        self.target_status = self.PLAYING
        current = self.player.get_state(0)[1]
        if current == gst.STATE_PAUSED:
            self.player.set_state(gst.STATE_PLAYING)
        elif current != gst.STATE_PLAYING :
            self.player.set_state(gst.STATE_PAUSED)
            self.status = self.PREROLLING

    def stop(self):
        self.player.do_cleanup()
        if self.status != self.STOPPED:
            self.status = self.STOPPED
            self.target_status = self.status
        gst.info("stopped player")

    def get_state(self, timeout=1):
        return self.player.get_state(timeout=timeout)

    def is_playing(self):
        return self.playing

class PlayerTUI():
    def __init__(self, args):

        self.player = GstPlayer()

        self.media_list = args
        self.media_idx = 1
        self.media_count = len(args)
        self.stress_count = 0

        def on_eos():
            self.player.stop()
            self.media_idx = self.media_idx + 1
            if self.media_idx < self.media_count:
                self.player.open_program(self.media_list[self.media_idx])
                self.player.play()
            else:
                self.quit()

        def on_error():
            self.player.stop()
            self.media_idx = self.media_idx + 1
            if self.media_idx < self.media_count:
                self.player.open_program(self.media_list[self.media_idx])
                self.player.play()
            else:
                self.media_idx = 1
                self.player.open_program(self.media_list[self.media_idx])
                self.player.play()

        print "q: quit"
        print "n: next clip"
        print "S: stress test for pipeline state changes"
        
        self.player.on_eos = lambda *x: on_eos()
        self.player.on_error = lambda *x: on_error()
        # The MainLoop
        self.mainloop = gobject.MainLoop()
        gobject.io_add_watch(sys.stdin, gobject.IO_IN, self.on_stdin)

        self.player.open_program(self.media_list[self.media_idx])
        self.player.play()
        try:
            self.mainloop.run()
        except KeyboardInterrupt:
            self.quit()

    def on_stdin(self, fd, condition):
        c = os.read(fd.fileno(), 1)

        if c == "q":
            self.quit()
        elif c == "n":
            self.next_clip()
        elif c == "S":
            gobject.timeout_add (4000, self.stress_test,
                priority = gobject.PRIORITY_HIGH)

        return True

    def quit(self):
        self.player.stop()
        self.player.do_cleanup()
        gst.info("finished cleanup")
        del self.player
        self.player = None
        self.mainloop.quit()

    def next_clip(self):
        self.player.stop()
        self.media_idx = self.media_idx + 1
        if self.media_idx < self.media_count:
            self.player.open_program(self.media_list[self.media_idx])
            self.player.play()
        else:
            self.media_idx = 1
            self.player.open_program(self.media_list[self.media_idx])
            self.player.play()

    def stress_test(self):
        self.stress_count = self.stress_count + 1
        print "Test #%d" % self.stress_count
        self.next_clip()
        return True

def main(args):
    def usage():
        sys.stderr.write("usage: %s list of programs\n" % args[0])
        sys.exit(1)

    if len(args) < 2:
        usage()

    tui = PlayerTUI(args)

if __name__ == '__main__':
    sys.exit(main(sys.argv))
